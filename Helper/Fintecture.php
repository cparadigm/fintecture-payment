<?php

namespace Fintecture\Payment\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Helper\AbstractHelper;

class Fintecture extends AbstractHelper
{
    /**
     * Checkout Session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * Qoute Model
     *
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;

    /**
     * QuoteManagement Model
     *
     * @var \Magento\Quote\Model\QuoteManagement
     */
    protected $quoteManagement;


    /**
     *
     * @param \Magento\Framework\Model\Context                   $context
     * @param \Magento\Framework\Registry                        $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory  $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory       $customAttributeFactory
     * @param \Magento\Payment\Helper\Data                       $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger               $logger
     * @param \Fintecture\Payment\Helper\Fintecture              $helper
     * @param \Magento\Checkout\Model\Session                    $checkoutSession
     * @param array                                              $data
     */
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $session,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Model\QuoteManagement $quoteManagement
    ) {
        $this->session = $session;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        parent::__construct($context);
    }


    /**
     * Restore Qoute Session
     *
     * @return void
     */
    public function restoreQuote()
    {
        return $this->session->restoreQuote();
    }

    /**
     * GET URL
     *
     * @return string
     */
    public function getUrl($route, $params = [])
    {
        return $this->_getUrl($route, $params);
    }

    public function getOrderStatusBasedOnPaymentStatus($apiResponse)
    {
        try {
            if (isset($apiResponse['meta']['status'])) {
                $rtn = array();
                switch ($apiResponse['meta']['status']) {

                case 'payment_created':
                    $rtn['state'] = \Magento\Sales\Model\Order::STATE_PROCESSING;
                    $rtn['status'] = 'processing';
                    break;
                case 'payment_pending':
                    // email auth pending response?
                    $rtn['state'] = \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
                    $rtn['status'] = 'pending_payment';
                    break;
                case 'payment_unsuccessful':
                case 'sca_required':
                case 'provider_required':
                case 'payment_error':
                    // state stays payment pending redirect to cart, message user
                    $rtn['state'] = \Magento\Sales\Model\Order::STATE_NEW;
                    $rtn['status'] = 'pending';
                    break;
                default:
                    $rtn['state'] = \Magento\Sales\Model\Order::STATE_NEW;
                    $rtn['status'] = 'pending';
                    break;
                }
            }
        } catch (\Exception $e) {
            $this->_finlogger->debug($e->getMessage());
        }

        return $rtn;
    }


    public function getStatusHistoryComment($apiResponse)
    {
        try {
            if (isset($apiResponse['meta']['status'])) {
                switch ($apiResponse['meta']['status']) {

                case 'payment_created':
                    return 'The payment initiation has been created successfully';
                    break;
                case 'payment_pending':
                    return 'payment pending';
                    break;
                case 'payment_unsuccessful':
                    return 'The buyer has either abandonned the payment flow on the banks web page, or the payment has not been accepted due to an authentication failure or insufficient funds in his bank account';
                    break;
                case 'sca_required':
                    return 'The buyer has selected a Bank in Connect and has been redirected to the Authentication page of the bank';
                    break;
                case 'provider_required':
                    return 'The buyer has select *Pay By Bank* as payment method, got redirected to Connect but has not selected any banks.';
                    break;
                case 'payment_error':
                    return 'Technical Error, the bank has rejected the payment initiation or has timeout';
                    break;
                default:
                    return 'payment pending';
                }
            }
        } catch (\Exception $e) {
            $this->_finlogger->debug($e->getMessage());
        }
    }
}
