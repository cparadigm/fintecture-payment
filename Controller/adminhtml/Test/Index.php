<?php
namespace Fintecture\Payment\Controller\Adminhtml\Test;

class Index extends \Magento\Backend\App\Action
{
    protected $fintectureModel;
    protected $jsonResultFactory;
    protected $scopeConfig;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Fintecture\Payment\Model\Fintecture $fintectureModel,
	\Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->fintectureModel = $fintectureModel;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute()
    {
	if( ! $fintecture_private_key = $this->fintectureModel->getAppPrivateKey() ) {
	    $this->messageManager->addError(__('No private key file found'));
	    throw new \Magento\Framework\Exception\LocalizedException(__('No private key file found'));
        }

	$fintecture_connect=$this->getRequest()->getParams();
	$env = $this->scopeConfig->getValue('payment/fintecture/environment',
		\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

	if ($env == 'sandbox') {
		$fintecture_connect_url=$this->scopeConfig
		->getValue('payment/fintecture/fintecture_connect_url_sandbox',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		
		$fintecture_oauth_url=$this->scopeConfig
		->getValue('payment/fintecture/fintecture_oauth_url_sandbox',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		
		$fintecture_pis_url=$this->scopeConfig
		->getValue('payment/fintecture/fintecture_pis_url_sandbox',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	} else {
		$fintecture_connect_url=$this->scopeConfig
		->getValue('payment/fintecture/fintecture_connect_url_production',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

		$fintecture_oauth_url=$this->scopeConfig
		->getValue('payment/fintecture/fintecture_oauth_url_production',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		
		$fintecture_pis_url=$this->scopeConfig
		->getValue('payment/fintecture/fintecture_pis_url_production',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

        $fintecture_app_secret=$fintecture_connect['fintecture_app_secret'];
        $fintecture_app_id=$fintecture_connect['fintecture_app_id'];

        $basic_token=$fintecture_app_id.':'.$fintecture_app_secret;
        $basic_token_data=base64_encode($basic_token);
        $postfields = "grant_type=client_credentials&scope=PIS&app_id=$fintecture_app_id";
        $data = array(
        'grant_type' => 'client_credentials',
        'scope' => 'PIS',
        'app_id' => $fintecture_app_id
        );
        $x_date = date('r');
        $digest = 'SHA-256=' . base64_encode(hash("sha256", json_encode($data, JSON_UNESCAPED_UNICODE), true));
        $x_request_id = $this->gen_uuid();

        $signing_string = '';
        $signing_string .= 'digest: ' . $digest . "\n";
        $signing_string .= 'x-date: ' . $x_date . "\n";
        $signing_string .= 'x-request-id: ' . $x_request_id;
        openssl_sign($signing_string, $crypted_string, $fintecture_private_key, OPENSSL_ALGO_SHA256);

        $signature = 'keyId="' . $fintecture_app_id . '",algorithm="rsa-sha256",headers="digest x-date x-request-id",signature="' . base64_encode($crypted_string) . '"';

        $curl = curl_init();

        curl_setopt_array(
            $curl,
            array(
            CURLOPT_URL => $fintecture_oauth_url."/oauth/secure/accesstoken/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HTTPHEADER => array(
            "signature: $signature",
            "accept: application/json",
            "authorization: Basic ".$basic_token_data,
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "digest: $digest",
            "app_id: $fintecture_app_id",
            "x-date: $x_date",
            "x-request-id: $x_request_id",
            // "postman-token: 8ba83449-947a-3736-cbbc-547f4ea7d3c9"
            ),
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $resultJson = $this->jsonResultFactory->create();
        return $resultJson->setData($response);
    }

    /**
     * Function to generate a Universal Unique ID (UUID)
     *
     * @return string - a UUID
     */
    public function gen_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            random_int(0, 0xffff),
            random_int(0, 0xffff),
            // 16 bits for "time_mid"
            random_int(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            random_int(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            random_int(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            random_int(0, 0xffff),
            random_int(0, 0xffff),
            random_int(0, 0xffff)
        );
    }
}
