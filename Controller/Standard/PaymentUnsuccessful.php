<?php

namespace Fintecture\Payment\Controller\Standard;

class PaymentUnsuccessful extends \Fintecture\Payment\Controller\WebhookAbstract
{
    public function execute()
    {
        try {
            if ($this->validateWebhook()) {
                $body = file_get_contents('php://input');
                parse_str($body, $data);
                $fintecture_payment_session_id = isset($data['session_id']) ? $data['session_id'] : false;
                $fintecture_payment_status = isset($data['status']) ? $data['status'] : false;
                if ($fintecture_payment_session_id) {
                    $orderCollection = $this->orderCollectionFactory->create();
                    $orderCollection->addFieldToFilter('fintecture_payment_session_id', $fintecture_payment_session_id);

                    $orderHooked = $orderCollection->getFirstItem();

                    if ($orderHooked->getId()) {
                        $order = $this->_orderFactory->create()->load($orderHooked->getId());
                        $response = [];
                        $response['meta'] = array(

                            'session_id' => $fintecture_payment_session_id,
                            'status' => $fintecture_payment_status

                        );

                        $this->_paymentMethod->handleFailedTransaction($order, $response);
                    }
                }
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->_finlogger->debug('Webhook Response Error 1 '.$e->getMessage());
        } catch (\Exception $e) {
            $this->_finlogger->debug('Webhook Response Error 2 '. $e->getMessage());
        }
    }
}
