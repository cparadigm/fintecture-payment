<?php

namespace Fintecture\Payment\Controller\Standard;

class Redirect extends \Fintecture\Payment\Controller\FintectureAbstract
{


    /**
     * Send Payment Gateway URL in Ajax Response
     *
     * @return array
     */
    public function execute()
    {
        try {
            if (!$this->getRequest()->isAjax()) {
                $this->_cancelPayment();
                $this->_checkoutSession->restoreQuote();
                $this->getResponse()->setRedirect(
                    $this->getCheckoutHelper()->getUrl('checkout') . "#payment"
                );
            }

            $quote = $this->getQuote();
            $email = $this->getRequest()->getParam('email');
            if ($this->getCustomerSession()->isLoggedIn()) {
                $this->getCheckoutSession()->loadCustomerQuote();
                $quote->updateCustomerData($this->getQuote()->getCustomer());
            } else {
                $quote->setCustomerEmail($email);
            }

            if ($this->getCustomerSession()->isLoggedIn()) {
                $quote->setCheckoutMethod(\Magento\Checkout\Model\Type\Onepage::METHOD_CUSTOMER);
            } else {
                $quote->setCheckoutMethod(\Magento\Checkout\Model\Type\Onepage::METHOD_GUEST);
            }

            $quote->setCustomerEmail($email);
            $quote->save();

          


            $params = [];
            $params["url"] = $this->getPaymentMethod()->getPaymentGatewayRedirectUrl();

            return $this->resultJsonFactory->create()->setData($params);
        } catch (\Exception $e) {
            $this->_finlogger->debug('Redirect '.$e->getMessage());
            
            $this->_checkoutSession->restoreQuote();
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }
}
