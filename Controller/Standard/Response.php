<?php

namespace Fintecture\Payment\Controller\Standard;

class Response extends \Fintecture\Payment\Controller\FintectureAbstract
{
    public function execute()
    {
        $returnUrl = $this->getCheckoutHelper()->getUrl('checkout');
        try {
            $paymentMethod = $this->getPaymentMethod();

            $response = $paymentMethod->getLastPaymentStatusResponse();
            $order = $this->getOrder();

            $orderStatus = $this->getCheckoutHelper()->getOrderStatusBasedOnPaymentStatus($response);
            if ($orderStatus['status'] == \Magento\Sales\Model\Order::STATE_PROCESSING) {
                $returnUrl = $this->getCheckoutHelper()->getUrl('checkout/onepage/success');
                $paymentMethod->handleSuccessTransaction($order, $response);
                try {
                    $this->_checkoutSession->setFintectureState(null);
                    $quote = $this->getQuote();
                    $ordernum = $order->getIncrementId();

                    $this->_checkoutSession->setLastSuccessQuoteId($order->getQouteId());
                    $this->_checkoutSession->setLastQuoteId($order->getQouteId());
                    $this->_checkoutSession->setLastOrderId($order->getEntityId());
                
                    $order->save();
                    $quote->setIsActive(false);
                    $this->cart->truncate()->save();
                    $this->quoteRepository->save($quote);
                    $this->_checkoutSession->clearStorage();

                    // clear any previous messages
                    $this->messageManager->getMessages(true);
                    $this->messageManager->addSuccess(__('Your Order is confirmed'));
                    $this->messageManager->addSuccess("#$ordernum");
                    
                    $invoice = current($order->getInvoiceCollection()->getItems());
                    /*
                    if ($invoice) {
                    $this->invoiceSender->send($invoice);
                    }
                    */
                } catch (\Exception $e) {
                    $this->_finlogger->error($e);
                }
            } elseif ($orderStatus['state'] == \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT) {
                $returnUrl = $this->getCheckoutHelper()->getUrl('checkout/onepage/success');
                $paymentMethod->handleHoldedTransaction($order, $response);

                $this->_checkoutSession->setFintectureState(null);
                $this->messageManager->addSuccess(__('Payment was initiated but has not been confirmed yet. Merchant will send confirmation once the transaction is settled.'));
                //TODO:  email merchant admin
            } else {
                $paymentMethod->handleFailedTransaction($order, $response);
                $this->_checkoutSession->setFintectureState('failed');

                //Restore Quote Items
                $this->_checkoutSession->restoreQuote();
                //        $order->cancel()->save();
                $returnUrl = $this->getCheckoutHelper()->getUrl('checkout') . "#payment";
                $this->messageManager->addError(__('The payment was unsuccessful. Please choose a different bank or different payment method.'));
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->_finlogger->debug('Response Error 1 '.$e->getMessage());
            $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
        } catch (\Exception $e) {
            $this->_finlogger->debug('Response Error 2 '. $e->getMessage());
            $this->messageManager->addExceptionMessage($e, __('We can\'t place the order.'));
        }

        $this->getResponse()->setRedirect($returnUrl);
    }
}
