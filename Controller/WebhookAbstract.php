<?php
namespace Fintecture\Payment\Controller;

abstract class WebhookAbstract extends \Magento\Framework\App\Action\Action implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    
    /**
     * @var \Fintecture\Payment\Logger
     */
    protected $_finlogger;

    /**
     * @var \Fintecture\Payment\Model\Fintecture
     */
    protected $_paymentMethod;

    /**
     * @var \Fintecture\Payment\Helper\Fintecture
     */
    protected $_checkoutHelper;


    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     */
    protected $orderCollectionFactory;
  
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Fintecture\Payment\Logger\Logger $finlogger,
        \Fintecture\Payment\Model\Fintecture $paymentMethod,
        \Fintecture\Payment\Helper\Fintecture $checkoutHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    ) {
        $this->_orderFactory = $orderFactory;
        $this->_paymentMethod = $paymentMethod;
        $this->_checkoutHelper = $checkoutHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_finlogger = $finlogger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context);
    }
    
    public function validateWebhook()
    {
        $body = file_get_contents('php://input');
        parse_str($body, $data);
        
        if (!count($data)) {
            return false;
        }

        $pkeyid = openssl_pkey_get_private($this->_paymentMethod->getAppPrivateKey());

        if ($pkeyid === false) {
            return false;
        }
        $digest = 'SHA-256=' . base64_encode(hash('sha256', $body, true));


        if (($digest === $_SERVER["HTTP_DIGEST"])) {
            $signingString = "date: ".$_SERVER["HTTP_DATE"]."\ndigest: ".$digest."\nx-request-id: ".$_SERVER["HTTP_X_REQUEST_ID"];

            $signature = str_replace("\\\"", "", $_SERVER["HTTP_SIGNATURE"]);
            $signature = explode(",", $signature);
            openssl_private_decrypt(base64_decode(explode("=", $signature[3])[1]), $decrypted, $pkeyid, OPENSSL_PKCS1_OAEP_PADDING);

            if (($signingString === $decrypted)) {
                return true;
            }
        }

        return false;
    }

    /**

     * @api

     * @return string (JSON)
     */
    public function execute()
    {
        return array('success'=>false);
    }
}
