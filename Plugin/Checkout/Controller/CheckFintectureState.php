<?php

namespace Fintecture\Payment\Plugin\Checkout\Controller;

class CheckFintectureState
{
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Fintecture\Payment\Helper\Fintecture
     */
    protected $checkoutHelper;
    
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Checkout\Model\Session                    $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator     $formKeyValidator
     * @param \Magento\Checkout\Model\Cart                       $cart
     * @param \Magento\Framework\View\Result\PageFactory         $resultPageFactory
     * @param \Magento\Sales\Model\OrderFactory                  $orderFactory
     * @param \Fintecture\Payment\Helper\Fintecture              $checkoutHelper
     * @param \Fintecture\Payment\Model\Order                    $finOrder
     * @param \Magento\Framework\App\Response\RedirectInterface  $redirect
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Fintecture\Payment\Helper\Fintecture $checkoutHelper,
        \Fintecture\Payment\Model\Order $finOrder,
	\Magento\Framework\App\Response\RedirectInterface $redirect
    ) {
	$this->storeManager = $storeManager;
        $this->orderFactory = $orderFactory;
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
        $this->finOrder = $finOrder;
        $this->redirect = $redirect;
    }

    public function beforeExecute($subject)
    {
	$quote = $this->checkoutSession->getQuote();
        $payment_method = $quote->getPayment()->getMethod();
	$order = $this->getOrder();
        if ( null == $payment_method // payment type not available on bank site
            && 'created' == $this->checkoutSession->getFintectureState()
	    && 'pending' == $order->getStatus()
	) {
	    $this->checkoutSession->restoreQuote();
            $returnUrl = $this->checkoutHelper->getUrl('checkout') . "#payment";
	    $this->checkoutSession->setFintectureState(null);
            $subject->getResponse()->setRedirect($returnUrl);
	} elseif ('fintecture' == $payment_method
	    && 'created' == $this->checkoutSession->getFintectureState()
	    && 'pending' == $order->getStatus()
	) {
	    $this->checkoutSession->restoreQuote();
	    $returnUrl = $this->checkoutHelper->getUrl('checkout') . "#payment";
	    $this->checkoutSession->setFintectureState(null);
	    $subject->getResponse()->setRedirect($returnUrl);
	} elseif ('fintecture' == $payment_method
	    &&    'created' == $this->checkoutSession->getFintectureState()
	    && 'pending' != $order->getStatus()
	) {
	    $this->checkoutSession->clearStorage();
	    $this->checkoutSession->setFintectureState(null);
	}
    }
    
    /**
     * Get order object
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function getOrder()
    {
        return $this->orderFactory->create()->loadByIncrementId(
            $this->checkoutSession->getLastRealOrderId()
        );
    }
}
