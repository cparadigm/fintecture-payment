<?php

namespace Fintecture\Payment\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
	
	$data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => 'payment/fintecture/fintecture_pis_url_sandbox',
            'value' => 'https://oauth-sandbox.fintecture.com',
    	];

  	$setup->getConnection()
           ->insertOnDuplicate($setup->getTable('core_config_data'), $data);
	
	$data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => 'payment/fintecture/fintecture_oauth_url_sandbox',
            'value' => 'https://oauth-sandbox.fintecture.com',
    	];
	
	$setup->getConnection()
           ->insertOnDuplicate($setup->getTable('core_config_data'), $data);

	$data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => 'payment/fintecture/fintecture_connect_url_sandbox',
            'value' => 'https://connect-sandbox.fintecture.com',
    	];
	
	$setup->getConnection()
		->insertOnDuplicate($setup->getTable('core_config_data'), $data);
	
	$data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => 'payment/fintecture/fintecture_oauth_url_production',
            'value' => 'https://oauth.fintecture.com',
    	];
	
	$setup->getConnection()
		->insertOnDuplicate($setup->getTable('core_config_data'), $data);
	
	$data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => 'payment/fintecture/fintecture_pis_url_production',
            'value' => 'https://api.fintecture.com',
    	];
	
	$setup->getConnection()
		->insertOnDuplicate($setup->getTable('core_config_data'), $data);

	$data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => 'payment/fintecture/fintecture_connect_url_production',
            'value' => 'https://connect.fintecture.com',
    	];
	
	$setup->getConnection()
		->insertOnDuplicate($setup->getTable('core_config_data'), $data);
        $installer->endSetup();
    }
}
