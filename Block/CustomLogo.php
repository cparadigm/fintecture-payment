<?php
namespace Fintecture\Payment\Block;

class CustomLogo extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param array                                   $data
     */

    protected $_storeManager;
    protected $_urlInterface;
    protected $moduleReader;
    protected $scopeConfig;
    const CUSTOM_LOGO_TYPE = 'payment/fintecture/general/show_logo';
    const CUSTOM_LOGO_POSITION = 'payment/fintecture/general/logo_position';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_urlInterface = $urlInterface;
        $this->moduleReader = $moduleReader;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * Get form action URL for POST booking request
     *
     * @return string
     */
    public function getImageUrl()
    {
	$getmediaUrl = $this->getViewFileUrl('Fintecture_Payment::images');
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

       
        $logo_type=$this->scopeConfig->getValue(self::CUSTOM_LOGO_TYPE, $storeScope);

        if ($logo_type=='Short') {
            $image_url=$getmediaUrl.'/29x29_square_gif.gif';
        } else {
            $image_url=$getmediaUrl.'/133x29_horizontal_gif.gif';
        }

        return $image_url;
    }

    public function getLogoType()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return  $this->scopeConfig->getValue(self::CUSTOM_LOGO_TYPE, $storeScope);
    }

    public function getLogoposition()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return  $this->scopeConfig->getValue(self::CUSTOM_LOGO_POSITION, $storeScope);
    }
}
