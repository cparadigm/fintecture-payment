<?php

namespace Fintecture\Payment\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Button extends Field
{
    protected $_template = 'Fintecture_Payment::system/config/button.phtml';
    public $scopeConfig;
    public $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
    }

    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getCustomUrl()
    {
        return $this->getUrl('fintecture/test/Index');
    }

    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'btn_id',
                'label' => __('Test Connection')
            ]
        );
        return $button->toHtml();
    }
    
    public function getSuccessResponse()
    {
        return __("Connection succeeded");
    }
    
    public function getFailureResponse()
    {
        return __("Connection did not succeed. Make sure to Save Config before testing.");
    }
}
