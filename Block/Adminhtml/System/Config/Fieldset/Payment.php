<?php

namespace Fintecture\Payment\Block\Adminhtml\System\Config\Fieldset;

/**
 * Fieldset renderer
 */
class Payment extends \Magento\Config\Block\System\Config\Form\Fieldset
{
    /**
     * @var \Magento\Config\Model\Config
     */
    public $_backendConfig;

    public $storeManager;

    /**
     * @param \Magento\Backend\Block\Context      $context
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\View\Helper\Js   $jsHelper
     * @param \Magento\Config\Model\Config        $backendConfig
     * @param array                               $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\View\Helper\Js $jsHelper,
        \Magento\Config\Model\Config $backendConfig,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->_backendConfig = $backendConfig;

        $this->_storeManager = $storeManager;
        parent::__construct($context, $authSession, $jsHelper, $data);
    }

    /**
     * Add custom css class
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function _getFrontendClass($element)
    {
        $enabledString = $this->_isPaymentEnabled($element) ? ' enabled' : '';
        return parent::_getFrontendClass($element) . ' with-button' . $enabledString;
    }

    /**
     * Check whether current payment method is enabled
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return bool
     */
    public function _isPaymentEnabled($element)
    {
        $groupConfig = $element->getGroup();
        $activityPaths = isset($groupConfig['activity_path']) ? $groupConfig['activity_path'] : [];

        if (!is_array($activityPaths)) {
            $activityPaths = [$activityPaths];
        }

        $isPaymentEnabled = false;
        foreach ($activityPaths as $activityPath) {
            $isPaymentEnabled = $isPaymentEnabled
            || (bool)(string)$this->_backendConfig->getConfigDataValue($activityPath);
        }

        return $isPaymentEnabled;
    }

    /**
     * Return header title part of html for payment solution
     *
     * @param                                   \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return                                  string
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function _getHeaderTitleHtml($element)
    {
        $mediaUrl=$this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $html = '<div class="config-heading fintecture_section" >';

        $storeCode = (int) $this->getRequest()->getParam('store', 0);

	$html .= '<div class="fintecture_logo" ><img src="' . $this->getViewFileUrl('Fintecture_Payment::images/icon_white-bg.jpg') . '"></div>';

        $html .= '<div class="fintecture_title" ><b>'. __('Pay with Instant Transfer').'</b><div class="fintecture_tagline" >'. __('Pay instantly and securely directly from your bank account. Collect payments without any credit limit. Reduce your transaction fees by 40% !').'</div></div>';

        $groupConfig = $element->getGroup();

        $disabledAttributeString = $this->_isPaymentEnabled($element) ? '' : ' disabled="disabled"';
        $disabledClassString = $this->_isPaymentEnabled($element) ? '' : ' disabled';
        $htmlId = $element->getHtmlId();
        $html .= '<div class="button-container">
			<button type="button"' .
        $disabledAttributeString .
        ' class="button action-configure' .
        (empty($groupConfig['checkout_com_separator']) ? '' : ' checkout-com-separator') .
        $disabledClassString .
        '" id="' .
        $htmlId .
        '-head" onclick="ckoToggleSolution.call(this, \'' .
        $htmlId .
        "', '" .
        $this->getUrl(
            'adminhtml/*/state'
        ) . '\'); return false;"><span class="state-closed">' . __(
            'Configure'
        ) . '</span><span class="state-opened">' . __(
            'Close'
        ) . '</span></button>';

        if (!empty($groupConfig['more_url'])) {
            $html .= '<a class="link-more" href="' . $groupConfig['more_url'] . '" target="_blank">' . __(
                'Learn More'
            ) . '</a>';
        }
        if (!empty($groupConfig['demo_url'])) {
            $html .= '<a class="link-demo" href="' . $groupConfig['demo_url'] . '" target="_blank">' . __(
                'View Demo'
            ) . '</a>';
        }

        $html .= '</div>';
        $html .= '<div class="heading"><strong>' . $element->getLegend() . '</strong>';

        if ($element->getComment()) {
            $html .= '<span class="heading-intro">' . $element->getComment() . '</span>';
        }
        $html .= '<div class="config-alt"></div>';
        $html .= '</div></div>';

        return $html;
    }

    /**
     * Return header comment part of html for payment solution
     *
     * @param                                         \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return                                        string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function _getHeaderCommentHtml($element)
    {
        return '';
    }

    /**
     * Get collapsed state on-load
     *
     * @param                                         \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return                                        false
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function _isCollapseState($element)
    {
        return false;
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function _getExtraJs($element)
    {
        $script = "require(['jquery', 'prototype'], function(jQuery){
			window.ckoToggleSolution = function (id, url) {
				var doScroll = false;
				Fieldset.toggleCollapse(id, url);
				if ($(this).hasClassName(\"open\")) {
					$$(\".with-button button.button\").each(function(anotherButton) {
						if (anotherButton != this && $(anotherButton).hasClassName(\"open\")) {
							$(anotherButton).click();
							doScroll = true;
	}
	}.bind(this));
	}
	jQuery('#payment_fr_fintecture_payment_options_specificcountry').on('change', function() {
		var applicable_countey=jQuery('#payment_fr_fintecture_payment_options_allowspecific').val();
		if (applicable_countey==1){

			if (this.value!='FR'){
				alert('Only French banks are supported at this time.');
	}
	}

	});

	jQuery('#payment_fr_fintecture_payment_options_allowspecific').on('change', function() {
		var applicable_countey=jQuery('#payment_fr_fintecture_payment_options_specificcountry').val();
		if (this.value==1){

			if (applicable_countey!='FR'){
				alert('Only French banks are supported at this time.');
	}
	}

	});

	jQuery('#payment_fr_fintecture_general-head').click(function(){



		var el = document.getElementById('payment_fr_fintecture_general_custom_file_upload_sandbox');

		if (jQuery('#payment_fr_fintecture_general_custom_file_upload_sandbox').next().is('div')==false) {

			el.classList.add('required-entry');
	}
else {

	el.classList.remove('required-entry');

	}



	if (jQuery('#payment_fr_fintecture_general_custom_file_upload_production').next().is('div')) {
		jQuery('#payment_fr_fintecture_general_custom_file_upload_production').removeClass('required-entry');
	}
else {
	jQuery('#payment_fr_fintecture_general_custom_file_upload_production').addClass('required-entry');
	}

	});

	if (doScroll) {
		var pos = Element.cumulativeOffset($(this));
		window.scrollTo(pos[0], pos[1] - 45);
	}
	jQuery('#payment_us_fintecture_general_fintecture_pis_url_sandbox').prop('disabled', true);
	jQuery('#payment_us_fintecture_general_title').prop('disabled', true);
	jQuery('#payment_us_fintecture_general_fintecture_oauth_url_sandbox').prop('disabled', true);
	jQuery('#payment_us_fintecture_general_fintecture_connect_url_sandbox').prop('disabled', true);
	jQuery('#payment_us_fintecture_general_fintecture_pis_url_production').prop('disabled', true);
	jQuery('#payment_us_fintecture_general_fintecture_oauth_url_production').prop('disabled', true);
	jQuery('#payment_us_fintecture_general_fintecture_connect_url_production').prop('disabled', true);
	jQuery('#row_payment_fr_fintecture_general_upload_image_id').prop('hidden', true);
	jQuery('#row_payment_fr_fintecture_general_upload_image_long_logo').prop('hidden', true);
	jQuery('#payment_fr_fintecture_general_title').prop('disabled', true);

	jQuery('#row_payment_fr_fintecture_general_fintecture_pis_url_sandbox').prop('hidden', true);
	jQuery('#row_payment_fr_fintecture_general_fintecture_oauth_url_sandbox').prop('hidden', true);
	jQuery('#row_payment_fr_fintecture_general_fintecture_connect_url_sandbox').prop('hidden', true);

	jQuery('#row_payment_fr_fintecture_general_fintecture_pis_url_production').prop('hidden', true);
	jQuery('#row_payment_fr_fintecture_general_fintecture_oauth_url_production').prop('hidden', true);
	jQuery('#row_payment_fr_fintecture_general_fintecture_connect_url_production').prop('hidden', true);
	}
	});";

        return $this->_jsHelper->getScript($script);
    }
}
