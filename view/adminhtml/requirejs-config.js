var config = {
    paths: {
        'js-file-alias': 'Fintecture_Payment/js/custom'
    },
    shim: {
        'js-file-alias': {
            deps: ['jquery']
        }
    }
};
