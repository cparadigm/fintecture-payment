define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default'
    ],
    function (
        $,
        Parent
    ) {
        'use strict';
        return Parent.extend(
            {
                redirectAfterPlaceOrder: false, // this is optional
                /**
                 * After place order callback
                 */
                afterPlaceOrder: function () {
                    console.log("after place order");
                    // your super awesome code
                }
            }
        );
    }
);f
