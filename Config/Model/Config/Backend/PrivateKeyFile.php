<?php
 
namespace Fintecture\Payment\Config\Model\Config\Backend;

class PrivateKeyFile extends \Magento\Config\Model\Config\Backend\File
{
    /**
     * @return string[]
     */
    public function _getAllowedExtensions()
    {
        return ['pem'];
    }

    /**
     * Save uploaded file before saving config value
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        $file = $this->getFileData();
        
        if (!empty($file)) {
            if (!isset($file['name']) ) {
                throw new \Magento\Framework\Exception\LocalizedException(__('%1', 'Private Key file name was not specified'));
            }

            $uploadDir = $this->_getUploadDir();
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0755, true);
            }
            try {
                /**
 * @var Uploader $uploader
		 */
                $uploader = $this->_uploaderFactory->create(['fileId' => $file]);
                $uploader->setAllowedExtensions($this->_getAllowedExtensions());
                $uploader->setAllowRenameFiles(false);
                $uploader->addValidateCallback('size', $this, 'validateMaxSize');
                $result = $uploader->save($uploadDir);
	    } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(__('%1', $e->getMessage()));
            }

            $filename = $result['file'];
            if (($filename)) {
                if ($this->_addWhetherScopeInfo()) {
                    $filename = $this->_prependScopeInfo($filename);
                }
                $this->setValue($filename);
            }
        } else {
            if (is_array($value) && !empty($value['value'])) {
                $this->setValue($value['value']);
            } else {
                $this->unsValue();
            }
        }

        return $this;
    }

    

    /**
     * Return path to directory for upload file
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _getUploadDir()
    {
        $fieldConfig = $this->getFieldConfig();

        if (!array_key_exists('upload_dir', $fieldConfig)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The base directory to upload file is not specified.')
            );
        }

        if (is_array($fieldConfig['upload_dir'])) {
            $uploadDir = $fieldConfig['upload_dir']['value'];

            if (array_key_exists('scope_info', $fieldConfig['upload_dir'])
                && $fieldConfig['upload_dir']['scope_info']
            ) {
                $uploadDir = $this->_appendScopeInfo($uploadDir);
            }

            if (array_key_exists('config', $fieldConfig['upload_dir'])) {
                $uploadDir = $this->getUploadDirPath('/' . $uploadDir);
            }
        } else {
            $uploadDir = (string)$fieldConfig['upload_dir'];
        }

        
        return $uploadDir;
    }

    /**
     * Retrieve upload directory path
     *
     * @param  string $uploadDir
     * @return string
     * @since  100.1.0
     */
    public function getUploadDirPath($uploadDir)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $configReader = $objectManager->create('Magento\Framework\Module\Dir\Reader');
        return $configReader->getModuleDir('etc', 'Fintecture_Payment').$uploadDir;
    }
}
