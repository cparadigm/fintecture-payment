<?php
namespace Fintecture\Payment\Model\ResourceModel\Order;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'fin_order_collection';
    protected $_eventObject = 'fin_order_collection';
    protected $_eavAttribute;

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_eavAttribute = $eavAttribute;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Fintecture\Payment\Model\Order', 'Fintecture\Payment\Model\ResourceModel\Order');
    }

    public function getOpenOrders($quote)
    {
        $curr_code = $quote->getQuoteCurrencyCode();
        $email = $quote->getCustomerEmail();
        $date = $quote->getCreatedAt();
        $this->getSelect()
            ->where('main_table.state =?', 'new')
            ->where('main_table.status =?', 'pending')
            ->where('main_table.order_currency_code =?', $curr_code)
            ->where('main_table.customer_email =?', $email)
            ->order('main_table.updated_at DESC');

        $rtn = $this
        //            ->addFieldToFilter('fintecture_payment_session_id',array('notnull' => true))
        ->addFieldToFilter('created_at', array('gt' => $date));
        return $rtn;
    }
}
