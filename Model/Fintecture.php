<?php

namespace Fintecture\Payment\Model;

use Magento\Sales\Api\Data\TransactionInterface;

class Fintecture extends \Magento\Payment\Model\Method\AbstractMethod
{
    const PAYMENT_FINTECTURE_CODE = 'fintecture';
    const APP_PRIVATE_KEY = 'app_privateKey.pem';
    const PRODUCTION_APP_PRIVATE_KEY_UPLOAD_PATH = '/lib/app_private_key_production';
    const SANDBOX_APP_PRIVATE_KEY_UPLOAD_PATH = '/lib/app_private_key_sandbox';

    /**
     * Payment Method Code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_FINTECTURE_CODE;

    /**
     * Payment Helper
     *
     * @var \Fintecture\Payment\Helper\Fintecture
     */
    protected $helper;

    /**
     * Checkout Session Model
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Fintecture\Payment\Logger
     */
    protected $_finlogger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $coreSession;

    /**
     * @var
     */
    protected $orderSender;

    /**
     * @var
     */
    protected $invoiceSender;
     
    /**
     * @var
     */
    protected $scopeConfig;

    /**
     *
     * @param \Magento\Framework\Model\Context                      $context
     * @param \Magento\Framework\Registry                           $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory     $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory          $customAttributeFactory
     * @param \Magento\Payment\Helper\Data                          $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface    $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger                  $logger
     * @param \Fintecture\Payment\Helper\Fintecture                 $helper
     * @param \Magento\Checkout\Model\Session                       $checkoutSession
     * @param \Fintecture\Payment\Logger\Logger                     $finlogger
     * @param \Magento\Framework\Json\Helper\Data                   $jsonHelper
     * @param \Magento\Framework\Session\SessionManagerInterface    $coreSession
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender   $orderSender
     * @param \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Fintecture\Payment\Helper\Fintecture $helper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Fintecture\Payment\Logger\Logger $finlogger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
    ) {
        $this->helper = $helper;
        $this->checkoutSession = $checkoutSession;
        $this->_finlogger = $finlogger;
        $this->jsonHelper = $jsonHelper;
        $this->coreSession = $coreSession;
        $this->orderSender = $orderSender;
        $this->invoiceSender = $invoiceSender;
        $this->scopeConfig = $scopeConfig;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger
        );
    }
    
    /**
     * Create Payment URL
     *
     * @return string
     */
    public function buildCheckoutURL()
    {
        $this->validateConfigValue();
        $lastRealOrder =  $this->checkoutSession->getLastRealOrder();
        $billingAddress = $lastRealOrder->getBillingAddress();
        $shippingAddress = $lastRealOrder->getBillingAddress();
        $data = [
            
            'meta' => [
                // Info of the buyer
                'psu_name' => $billingAddress->getName(),
                'psu_email' => $billingAddress->getEmail(),
                'psu_phone' => $billingAddress->getTelephone(),
                'psu_address' => [
                    'street' => implode(' ', $billingAddress->getStreet()) ,
                    'zip' => $billingAddress->getPostcode(),
                    'city' => $billingAddress->getCity(),
                    'country' => $billingAddress->getCountryId(),
                ],
            ],
            'data' => [
                'type' => 'SEPA',
                'attributes' => [
                    'amount' => $lastRealOrder->getBaseTotalDue(),
                    'currency' => $lastRealOrder->getOrderCurrencyCode(),
                    'communication' => "FINTECTURE-". $lastRealOrder->getId()
                    ],
                ],
        ];
        
        try {
            $gatewayClient = $this->getGatewayClient();
            $state = $gatewayClient->gen_uuid();
            $redirectUrl = $this->getReturnUrl();
            $originUrl = $this->getOriginUrl();
            $psuType = $this->scopeConfig->getValue('payment/fintecture/general/bank_type', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $apiResponse = $gatewayClient->generateConnectURL($data, $redirectUrl, $originUrl, $psuType, $state);
            if ($apiResponse['error']['code'] < 0) {
                $this->_finlogger->debug('buildCheckoutURL '.json_encode($apiResponse['error'],JSON_UNESCAPED_UNICODE));
                $this->checkoutSession->restoreQuote();
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Sorry, something went wrong. Please try again later.')
                );
            } else {
                $session_id = isset($apiResponse['data']['session_id']) ? $apiResponse['data']['session_id'] : '';
                
                $lastRealOrder->setFintecturePaymentSessionId($session_id);
                $lastRealOrder->setFintecturePaymentCustomerId($session_id);
                $lastRealOrder->save();

                $this->coreSession->setPaymentSessionId($session_id);
                return $apiResponse['data']['connect_url'];
            }
        } catch (\Exception $e) {
            $this->_finlogger->debug($e->getMessage());
            
            $this->checkoutSession->restoreQuote();
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }

    public function handleSuccessTransaction($order, $response)
    {
        $this->_finlogger->debug('handleSuccessTransaction '.json_encode($response,JSON_UNESCAPED_UNICODE));
        
        if ($order->getId()) {
            $transRefNumber = isset($response['meta']['session_id'])? $response['meta']['session_id']: '';

            $payment_note   = isset($response['meta']['status'])? $response['meta']['status']:'';

            $additionalData = $this->jsonHelper->jsonEncode($response);
            $order->getPayment()->setTransactionId($transRefNumber);
            $order->getPayment()->setLastTransId($transRefNumber);
            $order->getPayment()->addTransaction(TransactionInterface::TYPE_ORDER);
            $order->getPayment()->setIsTransactionClosed(0);
            $order->getPayment()->setAdditionalInformation('payment_additional_info', $additionalData);
            $order->getPayment()->place();

            $orderStatus = $this->helper->getOrderStatusBasedOnPaymentStatus($response)['status'];
            $orderState = $this->helper->getOrderStatusBasedOnPaymentStatus($response)['state'];

            $order->setStatus($orderStatus);
            
            $note = __($this->helper->getStatusHistoryComment($response));

            $order->setState($orderState);
            $order->addStatusHistoryComment($note);
            $order->save();
	    if(! $order->getEmailSent() ) {
		    $this->orderSender->send($order);
	    }
            if (!$order->hasInvoices() && $order->canInvoice()) {
                $invoice = $order->prepareInvoice();
                if ($invoice->getTotalQty() > 0) {
                    $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                    $invoice->setTransactionId($order->getPayment()->getTransactionId());
                    $invoice->register();
                    $invoice->addComment(__('Automatic invoice.'), false);
                    $invoice->save();
                    $this->invoiceSender->send($invoice);
                }
            }
        }
    }

    public function handleFailedTransaction($order, $response)
    {
        $this->_finlogger->debug('handleFailedTransaction '.json_encode($response,JSON_UNESCAPED_UNICODE));

        if ($order->getId()) {
            try {
                $transRefNumber = isset($response['meta']['session_id'])? $response['meta']['session_id']: '';

                $payment_note   = isset($response['meta']['status'])? $response['meta']['status']:'';
                $additionalData = $this->jsonHelper->jsonEncode($response);
                $order->getPayment()->setTransactionId($transRefNumber);
                $order->getPayment()->setLastTransId($transRefNumber);
                $order->getPayment()->setAdditionalInformation('payment_additional_info', $additionalData);
                
                $note = __($this->helper->getStatusHistoryComment($response));

                $orderStatus = $this->helper->getOrderStatusBasedOnPaymentStatus($response)['status'];
                $orderState = $this->helper->getOrderStatusBasedOnPaymentStatus($response)['state'];

                $order->setState($orderState);
                $order->setStatus($orderStatus);
                $order->setCustomerNoteNotify(false);
                $order->addStatusHistoryComment($note);
                $order->save();
            } catch (\Exception $e) {
                $this->_finlogger->debug($e->getMessage());
            }
        }
    }

    public function handleHoldedTransaction($order, $response)
    {
        $this->_finlogger->debug('handleHoldedTransaction '.json_encode($response,JSON_UNESCAPED_UNICODE));

        if ($order->getId()) {
            try {
                $transRefNumber = isset($response['meta']['session_id'])? $response['meta']['session_id']: '';

                $payment_note   = isset($response['meta']['status'])? $response['meta']['status']:'';
                $additionalData = $this->jsonHelper->jsonEncode($response);
                $order->getPayment()->setTransactionId($transRefNumber);
                $order->getPayment()->setLastTransId($transRefNumber);
                $order->getPayment()->setAdditionalInformation('payment_additional_info', $additionalData);
                
                $note = __($this->helper->getStatusHistoryComment($response));

                $orderStatus = $this->helper->getOrderStatusBasedOnPaymentStatus($response)['status'];
                $orderState = $this->helper->getOrderStatusBasedOnPaymentStatus($response)['state'];
                
                $order->setState($orderState);
                $order->setStatus($orderStatus);
                $order->setCustomerNoteNotify(false);
                $order->addStatusHistoryComment($note);
                $order->save();
            } catch (\Exception $e) {
                $this->_finlogger->debug($e->getMessage());
            }
        }
    }

    public function getLastPaymentStatusResponse()
    {
        $lastPaymentSessionId = $this->coreSession->getPaymentSessionId();
        $gatewayClient = $this->getGatewayClient();
        
        $apiResponse = [];

        $apiResponse = $gatewayClient->getPayment($lastPaymentSessionId);
        return $apiResponse;
    }

    /**
     * Validate Gateway Config Values
     *
     * @return void
     */
    public function validateConfigValue()
    {
        if ($this->getOauthURL() == ''
            || $this->getPisURL() == ''
            || $this->getConnectURL() == ''
            || $this->getAppPrivateKey() == ''
            || $this->getAppId() == ''
            || $this->getAppSecret() == ''
        ) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong try another payment method!')
            );
        }
    }

    /**
     * Return url according to environment
     *
     * @return string
     */
    public function getPaymentGatewayRedirectUrl()
    {
        return $this->buildCheckoutURL();
    }

    /**
     * Initialize Gateway Object
     *
     * @return object
     */
    public function getGatewayClient()
    {
        $gatewayClient = new \Fintecture\Payment\Gateway\Client(
            [
                'FINTECTURE_OAUTH_URL' => $this->getOauthURL(),
                'FINTECTURE_PIS_URL' => $this->getPisURL(),
                'FINTECTURE_CONNECT_URL' => $this->getConnectURL(),
                'FINTECTURE_PRIVATE_KEY' => $this->getAppPrivateKey(),
                'FINTECTURE_APP_ID' => $this->getAppId(),
                'FINTECTURE_APP_SECRET' => $this->getAppSecret(),
            ]
        );
        return $gatewayClient;
    }


    /**
     * Return URL For Redirect Proccessing At Magento End
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->helper->getUrl('fintecture/standard/redirect');
    }
    
    public function getReturnUrl()
    {
        return $this->helper->getUrl('fintecture/standard/response');
    }
    
    public function getOriginUrl()
    {
        return $this->helper->getUrl('checkout/') . '#payment';
    }

    public function getApiEnvironment()
    {
        return $this->getConfigData('environment');
    }

    /**
     * Return Payment APP Oauth URL
     *
     * @return string
     */
    public function getOauthURL()
    {
        $env = $this->getApiEnvironment();

        if ($env === 'production') {
            return $this->getConfigData('fintecture_oauth_url_production');
        }

        return $this->getConfigData('fintecture_oauth_url_sandbox');
    }

    /**
     * Return Payment APP PIS URL
     *
     * @return string
     */
    public function getPisURL()
    {
        $env = $this->getApiEnvironment();

        if ($env === 'production') {
            return $this->getConfigData('fintecture_pis_url_production');
        }

        return $this->getConfigData('fintecture_pis_url_sandbox');
    }

    /**
     * Return Payment APP Connect URL
     *
     * @return string
     */
    public function getConnectURL()
    {
        $env = $this->getApiEnvironment();

        if ($env === 'production') {
            return $this->getConfigData('fintecture_connect_url_production');
        }

        return $this->getConfigData('fintecture_connect_url_sandbox');
    }


    /**
     * Return Client APP Id
     *
     * @return string
     */
    public function getAppId()
    {
        $env = $this->getApiEnvironment();

        if ($env === 'production') {
            return $this->getConfigData('fintecture_app_id_production');
        }

        return $this->getConfigData('fintecture_app_id_sandbox');
    }

    /**
     * Return Client APP Secret
     *
     * @return string
     */
    public function getAppSecret()
    {
        $env = $this->getApiEnvironment();

        if ($env === 'production') {
            return $this->getConfigData('fintecture_app_secret_production');
        }
        return $this->getConfigData('fintecture_app_secret_sandbox');
    }

    /**
     * Return APP Private Key Pem File Secret
     *
     * @return string
     */
    public function getAppPrivateKey()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $configReader = $objectManager->create('Magento\Framework\Module\Dir\Reader');
        $modulePath = $configReader->getModuleDir('etc', 'Fintecture_Payment');

        $env = $this->getApiEnvironment();
        if ($env === 'production') {
	    $fileDirPath = $modulePath . self::PRODUCTION_APP_PRIVATE_KEY_UPLOAD_PATH;
	    $fileName = $this->findKeyfile($fileDirPath);
            $fileAbsPath = $modulePath . self::PRODUCTION_APP_PRIVATE_KEY_UPLOAD_PATH . '/'. $fileName;
        } else {
            $fileDirPath = $modulePath . self::SANDBOX_APP_PRIVATE_KEY_UPLOAD_PATH;
	    $fileName = $this->findKeyfile($fileDirPath);
            $fileAbsPath = $modulePath . self::SANDBOX_APP_PRIVATE_KEY_UPLOAD_PATH . '/'. $fileName;
	}
	if ( ! $fileName ) {
		return false;
	}
        return preg_replace("/\n\r/m", '\n', file_get_contents($fileAbsPath));
    }

    private function findKeyfile($dir_path) {
	    $files = scandir($dir_path);
	    foreach($files as $file) {
		    if( strpos($file,'.pem') !== false) {
			    return $file;
		    }
	    }
	    return false;
    }

    /**
     * Return Client Beneficiary Name
     *
     * @return string
     */
    public function getBeneficiaryName()
    {
        return $this->getConfigData('beneficiary_name');
    }

    /**
     * Return Client Beneficiary Street Address
     *
     * @return string
     */
    public function getBeneficiaryStreet()
    {
        return $this->getConfigData('beneficiary_street');
    }

    /**
     * Return Client Beneficiary Number
     *
     * @return string
     */
    public function getBeneficiaryNumber()
    {
        return $this->getConfigData('beneficiary_number');
    }

    /**
     * Return Client Beneficiary City
     *
     * @return string
     */
    public function getBeneficiaryCity()
    {
        return $this->getConfigData('beneficiary_city');
    }

    /**
     * Return Client Beneficiary Zip
     *
     * @return string
     */
    public function getBeneficiaryZip()
    {
        return $this->getConfigData('beneficiary_zip');
    }

    /**
     * Return Client Beneficiary Country
     *
     * @return string
     */
    public function getBeneficiaryCountry()
    {
        return $this->getConfigData('beneficiary_country');
    }

    /**
     * Return Client Beneficiary IBAN
     *
     * @return string
     */
    public function getBeneficiaryIban()
    {
        return $this->getConfigData('beneficiary_iban');
    }

    /**
     * Return Client Beneficiary Swift BIC
     *
     * @return string
     */
    public function getBeneficiarySwiftBic()
    {
        return $this->getConfigData('beneficiary_swift_bic');
    }

    /**
     * Return Client Beneficiary Swift BIC
     *
     * @return string
     */
    public function getBeneficiaryBankName()
    {
        return $this->getConfigData('beneficiary_bank_name');
    }
}
