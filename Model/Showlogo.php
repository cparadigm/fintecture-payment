<?php
namespace Fintecture\Payment\Model;

class Showlogo implements \Magento\Framework\Option\ArrayInterface
{
    const LOGO_SHORT    = 'Short';
    const LOGO_LONG       = 'Long';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::LOGO_SHORT,
                'label' => __('Short'),
            ],
            [
                'value' => self::LOGO_LONG,
                'label' => __('Long')
            ]
        ];
    }
}
