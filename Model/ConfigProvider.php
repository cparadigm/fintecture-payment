<?php

namespace Fintecture\Payment\Model;

class ConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
   
    /**
     * Payment Method Code
     *
     * @var string
     */
    protected $methodCode = \Fintecture\Payment\Model\Fintecture::PAYMENT_FINTECTURE_CODE;
    
    protected $method;
    
    public function __construct(
        \Magento\Payment\Helper\Data $paymenthelper
    ) {
        $this->method = $paymenthelper->getMethodInstance($this->methodCode);
    }

    /**
     * Return Payment Gateway Config
     *
     * @return string
     */
    public function getConfig()
    {
        return $this->method->isAvailable() ? [
            'payment'=>['fintecture'=>[
                'redirectUrl'=>$this->method->getRedirectUrl()
            ]
            ]
        ]:[];
    }
}
