<?php
namespace Fintecture\Payment\Model;

class Banktype implements \Magento\Framework\Option\ArrayInterface
{
    const BANK_RETAIL    = 'Retail';
    const BANK_CORPORATE       = 'Corporate';
    const BANK_ALL       = 'All';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [

            [
                'value' => self::BANK_ALL,
                'label' => __('All')
            ],
            [
                'value' => self::BANK_RETAIL,
                'label' => __('Retail')
            ],
            [
                'value' => self::BANK_CORPORATE,
                'label' => __('Corporate')
            ]
        ];
    }
}
