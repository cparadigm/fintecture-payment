<?php

namespace  Fintecture\Payment\Model;

use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\Quote\Address\ToOrder as ToOrderConverter;
use Magento\Quote\Model\Quote\Address\ToOrderAddress as ToOrderAddressConverter;
use Magento\Quote\Model\Quote as QuoteEntity;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Quote\Model\Quote\Payment\ToOrderPayment as ToOrderPaymentConverter;
use Magento\Sales\Api\Data\OrderInterfaceFactory as OrderFactory;
use Magento\Sales\Api\OrderManagementInterface as OrderManagement;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Quote\Model\SubmitQuoteValidator;
use Magento\Quote\Model\CustomerManagement;
use Fintecture\Payment\Model\Order;

/**
 * Adds remote payment state checks to submitQuote function
 */
class QuoteManagement extends \Magento\Quote\Model\QuoteManagement
{
    /**
     * @var EventManager
     */
    protected $eventManager;

    /**
     * @var SubmitQuoteValidator
     */
    private $submitQuoteValidator;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var OrderManagement
     */
    protected $orderManagement;

    /**
     * @var CustomerManagement
     */
    protected $customerManagement;

    /**
     * @var ToOrderConverter
     */
    protected $quoteAddressToOrder;

    /**
     * @var ToOrderAddressConverter
     */
    protected $quoteAddressToOrderAddress;

    /**
     * @var ToOrderItemConverter
     */
    protected $quoteItemToOrderItem;

    /**
     * @var ToOrderPaymentConverter
     */
    protected $quotePaymentToOrderPayment;

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerModelFactory;

    /**
     * @var \Magento\Quote\Model\Quote\AddressFactory
     */
    protected $quoteAddressFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    private $quoteIdMaskFactory;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var array
     */
    private $addressesToSync = [];

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var Fintecture\Payment\Model\Order
     */
    private $finOrder;

    /**
     * @param EventManager                                          $eventManager
     * @param SubmitQuoteValidator                                  $submitQuoteValidator
     * @param OrderFactory                                          $orderFactory
     * @param OrderManagement                                       $orderManagement
     * @param CustomerManagement                                    $customerManagement
     * @param ToOrderConverter                                      $quoteAddressToOrder
     * @param ToOrderAddressConverter                               $quoteAddressToOrderAddress
     * @param ToOrderItemConverter                                  $quoteItemToOrderItem
     * @param ToOrderPaymentConverter                               $quotePaymentToOrderPayment
     * @param UserContextInterface                                  $userContext
     * @param \Magento\Quote\Api\CartRepositoryInterface            $quoteRepository
     * @param \Magento\Customer\Api\CustomerRepositoryInterface     $customerRepository
     * @param \Magento\Customer\Model\CustomerFactory               $customerModelFactory
     * @param \Magento\Quote\Model\Quote\AddressFactory             $quoteAddressFactory
     * @param \Magento\Framework\Api\DataObjectHelper               $dataObjectHelper
     * @param StoreManagerInterface                                 $storeManager
     * @param \Magento\Checkout\Model\Session                       $checkoutSession
     * @param \Magento\Customer\Model\Session                       $customerSession
     * @param \Magento\Customer\Api\AccountManagementInterface      $accountManagement
     * @param QuoteFactory                                          $quoteFactory
     * @param \Magento\Quote\Model\QuoteIdMaskFactory|null          $quoteIdMaskFactory
     * @param \Magento\Customer\Api\AddressRepositoryInterface|null $addressRepository
     * @param \Magento\Framework\App\RequestInterface|null          $request
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress  $remoteAddress
     * @param \Fintecture\Payment\Model\Order
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        EventManager $eventManager,
        SubmitQuoteValidator $submitQuoteValidator,
        OrderFactory $orderFactory,
        OrderManagement $orderManagement,
        CustomerManagement $customerManagement,
        ToOrderConverter $quoteAddressToOrder,
        ToOrderAddressConverter $quoteAddressToOrderAddress,
        ToOrderItemConverter $quoteItemToOrderItem,
        ToOrderPaymentConverter $quotePaymentToOrderPayment,
        UserContextInterface $userContext,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerModelFactory,
        \Magento\Quote\Model\Quote\AddressFactory $quoteAddressFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Fintecture\Payment\Model\Order $finOrder,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory = null,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository = null,
        \Magento\Framework\App\RequestInterface $request = null,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress = null
    ) {
        parent::__construct(
            $eventManager,
            $submitQuoteValidator,
            $orderFactory,
            $orderManagement,
            $customerManagement,
            $quoteAddressToOrder,
            $quoteAddressToOrderAddress,
            $quoteItemToOrderItem,
            $quotePaymentToOrderPayment,
            $userContext,
            $quoteRepository,
            $customerRepository,
            $customerModelFactory,
            $quoteAddressFactory,
            $dataObjectHelper,
            $storeManager,
            $checkoutSession,
            $customerSession,
            $accountManagement,
            $quoteFactory,
            $quoteIdMaskFactory,
            $addressRepository,
            $request,
            $remoteAddress
        );
        $this->submitQuoteValidator = $submitQuoteValidator;
        $this->finOrder = $finOrder;
    }

    /**
     * Submit quote
     *
     * @param  Quote $quote
     * @param  array $orderData
     * @return \Magento\Framework\Model\AbstractExtensibleModel|\Magento\Sales\Api\Data\OrderInterface|object
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function submitQuote(QuoteEntity $quote, $orderData = [])
    {
        $payment_method = $quote->getPayment()->getMethod();
        $fintecture_state = $this->checkoutSession->getFintectureState();
        $this->updateState($payment_method);

        // define $order to be conditionally populated
        $order = null;
        $useExistingOrder = $this->useExistingOrder($payment_method, $fintecture_state);

        if ($useExistingOrder) {
            $order = $this->finOrder->getOrderForQuote($quote);
            return $order;
        }

        $order = $this->orderFactory->create();
        $this->submitQuoteValidator->validateQuote($quote);
        if (!$quote->getCustomerIsGuest()) {
            if ($quote->getCustomerId()) {
                $this->_prepareCustomerQuote($quote);
                $this->customerManagement->validateAddresses($quote);
            }
            $this->customerManagement->populateCustomerInfo($quote);
        }
        $addresses = [];
        $quote->reserveOrderId();
        if ($quote->isVirtual()) {
            $this->dataObjectHelper->mergeDataObjects(
                \Magento\Sales\Api\Data\OrderInterface::class,
                $order,
                $this->quoteAddressToOrder->convert($quote->getBillingAddress(), $orderData)
            );
        } else {
            $this->dataObjectHelper->mergeDataObjects(
                \Magento\Sales\Api\Data\OrderInterface::class,
                $order,
                $this->quoteAddressToOrder->convert($quote->getShippingAddress(), $orderData)
            );
            $shippingAddress = $this->quoteAddressToOrderAddress->convert(
                $quote->getShippingAddress(),
                [
                'address_type' => 'shipping',
                'email' => $quote->getCustomerEmail()
                ]
            );
            $shippingAddress->setData('quote_address_id', $quote->getShippingAddress()->getId());
            $addresses[] = $shippingAddress;
            $order->setShippingAddress($shippingAddress);
            $order->setShippingMethod($quote->getShippingAddress()->getShippingMethod());
        }
        $billingAddress = $this->quoteAddressToOrderAddress->convert(
            $quote->getBillingAddress(),
            [
            'address_type' => 'billing',
            'email' => $quote->getCustomerEmail()
            ]
        );
        $billingAddress->setData('quote_address_id', $quote->getBillingAddress()->getId());
        $addresses[] = $billingAddress;
        $order->setBillingAddress($billingAddress);
        $order->setAddresses($addresses);
        $order->setPayment($this->quotePaymentToOrderPayment->convert($quote->getPayment()));
        $order->setItems($this->resolveItems($quote));
        if ($quote->getCustomer()) {
            $order->setCustomerId($quote->getCustomer()->getId());
        }
        $order->setQuoteId($quote->getId());
        $order->setCustomerEmail($quote->getCustomerEmail());
        $order->setCustomerFirstname($quote->getCustomerFirstname());
        $order->setCustomerMiddlename($quote->getCustomerMiddlename());
        $order->setCustomerLastname($quote->getCustomerLastname());
        $this->submitQuoteValidator->validateOrder($order);

        $this->eventManager->dispatch(
            'sales_model_service_quote_submit_before',
            [
            'order' => $order,
            'quote' => $quote
            ]
        );
        try {
            $order = $this->orderManagement->place($order);
            $quote->setIsActive(false);
            $this->eventManager->dispatch(
                'sales_model_service_quote_submit_success',
                [
                'order' => $order,
                'quote' => $quote
                ]
            );
            $this->quoteRepository->save($quote);
        } catch (\Exception $e) {
            $this->rollbackAddresses($quote, $order, $e);
            throw $e;
        }
        return $order;
    }

    public function useExistingOrder($pymt_type, $fintecture_state)
    {
        try {
            if ($pymt_type === 'fintecture') {
                switch ($fintecture_state) {
                case null:
                    return false;
                case 'created':
                    return true;
                case 'failed':
                    return true;
                default:
                    return true;

                }
            } else {
                switch ($fintecture_state) {
                case null:
                    return false;
                case 'created':
                    return true;
                case 'failed':
                    return true;
                default:
                    return true;
                }
            }
        } catch (\Exception $e) {
            $this->_finlogger->debug($e->getMessage());
        }
    }

    public function updateState($pymt_type)
    {
        try {
            if ($pymt_type === 'fintecture') {
                $this->checkoutSession->setFintectureState('created');
            } else {
                $this->checkoutSession->setFintectureState(null);
            }
        } catch (\Exception $e) {
            $this->_finlogger->debug($e->getMessage());
        }

        return true;
    }
}
