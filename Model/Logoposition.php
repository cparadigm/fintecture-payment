<?php
namespace Fintecture\Payment\Model;

class Logoposition implements \Magento\Framework\Option\ArrayInterface
{
    const LEFT_OF_TITLE    = 'Left_of_title';
    const RIGHT_OF_TITLE       = 'Right_of_title';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::LEFT_OF_TITLE,
                'label' => __('Left of Title'),
            ],
            [
                'value' => self::RIGHT_OF_TITLE,
                'label' => __('Right of Title')
            ]
        ];
    }
}
