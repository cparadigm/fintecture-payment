<?php

namespace Fintecture\Payment\Observer\Quote;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Psr\Log\LoggerInterface;

/**
 * Class responsive for sending order and invoice emails when it's created through storefront.
 */
class SubmitObserver extends \Magento\Quote\Observer\SubmitObserver
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderSender
     */
    private $orderSender;

    /**
     * @var InvoiceSender
     */
    private $invoiceSender;

    /**
     * @param LoggerInterface $logger
     * @param OrderSender     $orderSender
     * @param InvoiceSender   $invoiceSender
     */
    public function __construct(
        LoggerInterface $logger,
        OrderSender $orderSender,
        InvoiceSender $invoiceSender
    ) {
        $this->logger = $logger;
        $this->orderSender = $orderSender;
        $this->invoiceSender = $invoiceSender;
    }

    /**
     * Send order and invoice email.
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /**
 * @var  Quote $quote
*/
        $quote = $observer->getEvent()->getQuote();
        /**
 * @var  Order $order
*/
        $order = $observer->getEvent()->getOrder();
        $payment_method = $order->getPayment()->getData('method');
        $time = date(DATE_RFC2822);
        /**
         * a flag to set that there will be redirect to third party after confirmation
         */
        $redirectUrl = $quote->getPayment()->getOrderPlaceRedirectUrl();
        if ($payment_method != 'fintecture' && !$redirectUrl && $order->getCanSendNewEmailFlag()) {
            try {
                $this->orderSender->send($order);
                $invoice = current($order->getInvoiceCollection()->getItems());
                if ($invoice) {
                    $this->invoiceSender->send($invoice);
                }
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }
}
